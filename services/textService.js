import textRepository from '../repositories/textRepository';

class TextService {
  constructor(repository) {
    this.repository = repository;
  }

  getTexts() {
    const texts = this.repository.getAll();
    return texts;
  }

  getText(id) {
    const text = this.repository.getOne({ id });
    if (!text) throw new Error('Text not found');
    return text;
  }

  createText(textData) {
    const createdText = this.repository.create(textData);
    if (!createdText) throw new Error('Text not created');
    return createdText;
  }

  updateText(id, textData) {
    if (!this.repository.getOne({ id })) {
      throw new Error("Text can't be updated, because not found");
    }
    const updatedText = this.repository.update(id, textData);
    if (!updatedText) throw new Error('Text not updated');
    return updatedText;
  }

  deleteText(id) {
    if (!this.repository.getOne({ id })) {
      throw new Error("Text can't be deleted, because not found");
    }
    const deletedText = this.repository.delete(id);
    if (!deletedText) throw new Error('Text not deleted');
    return deletedText;
  }
}

const texService = new TextService(textRepository);
export default texService;
