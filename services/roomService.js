/* eslint-disable no-console */
import roomRepository from '../repositories/roomRepository';

class RoomService {
  constructor(repository) {
    this.repository = repository;
  }

  getRooms() {
    const rooms = this.repository.getAll();
    if (!rooms) throw new Error('Rooms not found');
    return rooms;
  }

  getRoom(id) {
    const room = this.repository.getOne({ id });
    return room;
  }

  createRoom(roomData) {
    const createdRoom = this.repository.create(roomData);
    if (!createdRoom) throw new Error('Room not created');
    return createdRoom;
  }

  updateRoom(id, roomData) {
    if (!this.repository.getOne({ id })) {
      throw new Error("Room can't be updated, because not found");
    }
    const updatedRoom = this.repository.update(id, roomData);
    if (!updatedRoom) throw new Error('Room not updated');
    return updatedRoom;
  }

  deleteRoom(id) {
    if (!this.repository.getOne({ id })) {
      throw new Error("Room can't be deleted, because not found");
    }
    const deletedRoom = this.repository.delete(id);
    if (!deletedRoom) throw new Error('Room not deleted');
    return deletedRoom;
  }
}

const roomService = new RoomService(roomRepository);

export default roomService;
