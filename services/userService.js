/* eslint-disable no-proto */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
import userRepository from '../repositories/userRepository';

class UserService {
  constructor(repository) {
    this.repository = repository;
  }

  getUsers() {
    const users = this.repository.getAll();
    if (!users) throw new Error('Users not found');
    return users;
  }

  getUser(username) {
    const user = this.repository.getOne({ username });
    return user;
  }

  createUser(userData) {
    const createdUser = this.repository.create({
      ...userData,
      roomId: null
    });

    if (!createdUser) throw new Error('User not created');
    return createdUser;
  }

  updateUser(id, userData) {
    if (!this.repository.getOne({ id })) {
      throw new Error("User can't be updated, because not found");
    }
    const updatedUser = this.repository.update(id, userData);
    if (!updatedUser) throw new Error('User not updated');
    return updatedUser;
  }

  deleteUser(id) {
    if (!this.repository.getOne({ id })) {
      throw new Error("User can't be deleted, because not found");
    }
    const deletedUser = this.repository.delete(id);
    if (!deletedUser) throw new Error('User not deleted');
    return deletedUser;
  }
}

const userService = new UserService(userRepository);

export default userService;
