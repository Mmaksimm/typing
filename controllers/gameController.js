// import * as userService from '../services/userService';
import * as roomService from '../services/roomService';
// import * as textService from '../services/textService';

const startGame = async ({ roomId, id: userId }) => {
  const room = await roomService.getRoom(roomId);
  const updatedRoom = {
    ...room,
    users: room.users.map(user => {
      if (userId !== user.id) return user;
      return {
        ...user,
        game: true
      };
    })
  };

  if (updatedRoom.users.every(({ game }) => game)) updatedRoom.startGame = true;
  await roomService.updateRoom(roomId, updatedRoom);

  return {
    updatedRoom
  };
};

export {
  startGame
};
