/* eslint-disable consistent-return */
/* eslint-disable no-proto */
/* eslint-disable no-undef */
/* eslint-disable no-console */
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config/socketConfig';
import userService from '../services/userService';
import roomService from '../services/roomService';
// import * as textService from '../services/textService';

const createUser = async username => {
  const user = await userService.getUser(username);
  if (user) throw new Error('The user already exists with this name.');
  const createdUser = await userService.createUser({ username, roomId: null });
  if (!createdUser) throw new Error('User not created');
  const listRooms = await roomService.getRooms();
  const listFreeRooms = listRooms.filter(room => (
    !room.startGame && !room.game && room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM
  ));

  return {
    user: createdUser,
    rooms: listFreeRooms
  };
};

// eslint-disable-next-line consistent-return
const deleteUser = async username => {
  const { id, roomId } = await userService.getUser(username);
  await userService.deleteUser(id);
  if (!roomId) return;
  const room = await roomService.getRoom(id, roomId);
  const users = await room.users.filter(userId => id !== userId);
  if (users.length) {
    await roomService.updateRoom(roomId, { ...room, users });
    return { ...room, users };
  }

  await roomService.deleteRoom(roomId);
};

export {
  createUser,
  deleteUser
};
