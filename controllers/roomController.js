/* eslint-disable no-console */
import userService from '../services/userService';
import roomService from '../services/roomService';
// import  textService from '../services/textService';

const createRoom = async ({ roomName, username, userId }) => {
  const room = await roomService.createRoom({
    roomName,
    startGame: false,
    game: false,
    users: [
      {
        username,
        userId,
        game: false
      }
    ]
  });

  const updatedUser = await userService.updateUser(userId, { roomId: room.id });
  if (!updatedUser) throw new Error('User don\'t updated');

  return {
    room,
    user: updatedUser
  };
};

const enterRoom = async ({ roomId, username, userId }) => {
  const room = await roomService.getRoom(roomId);
  if (room.startGame && room.game) throw new Error('Game already started.');
  const updatedRoom = await roomService.updateRoom(roomId, {
    ...room,
    users: [
      ...room.users,
      {
        username,
        userId,
        game: false
      }
    ]
  });

  const updatedUser = await userService.updateUser(userId, { roomId });

  return {
    room: updatedRoom,
    user: updatedUser
  };
};

const leaveRoom = async ({ roomId, userId }) => {
  const updatedUser = await userService.updateUser(userId, { roomId: null });
  const room = roomService.getRoom(roomId);
  if (room.users.length === 1) {
    await roomService.deleteRoom(roomId);
    const roomList = await roomService.getRooms();
    return {
      user: updatedUser,
      rooms: roomList
    };
  }

  await roomService.updateRoom(roomId, {
    ...room, users: room.users.filter(user => user.userId !== userId)
  });
  const roomList = await roomService.getRooms();
  return {
    user: updatedUser,
    rooms: roomList
  };
};

export {
  createRoom,
  enterRoom,
  leaveRoom
};
