/* eslint-disable no-confusing-arrow */
/* eslint-disable prefer-template */
/* eslint-disable default-case */
/* eslint-disable no-unused-vars */
/* eslint-disable no-alert */
/* eslint-disable max-len */
/* eslint-disable quote-props */
/* eslint-disable no-console */

/* eslint-disable no-undef */
const username = sessionStorage.getItem('username');
const root = document.getElementById('root');
let data = {};

// render
const renderPreviewRooms = renderData => {
  const roomsPreview = renderData.rooms.map(room => `
<div class="card col-3 my-2 mx-4" style="display:inline-block">
  <div class="card-body">
${room.users.length} user${room.users.length === 1 ? '' : 's'} connected.
    <h5 class="card-title text-center py-5">${room.roomName}</h5>
    <div class="text-center">
        <a href="#" class="btn btn-primary" data-event="${room.id}">Join</a>
    </div>
  </div>
</div>
`).join('');

  // eslint-disable-next-line prefer-template
  root.innerHTML = '<div class="py-4 text-center"><a href="#" class="btn btn-primary" data-event="createRoom">Create room</a></div>' + roomsPreview;
};

const renderGameRoom = ({ activeRoom }) => {
  const playersPreview = activeRoom.users.map(user => `
  <div>
  <h5 class="py-2" id="${user.id}">
    <span class="text-danger">&ofcir;</span>
    ${user.username}
  </h5>
  <div class="progress">
    <div class="progress-bar"></div>
  </div>
</div>
  `).join('');

  const leftPanelPreview = '<div class="pl-2 pr-5"><div class="text-center py-5"><button type="button" class="btn btn-primary" data-event="backToRooms">&lt Back To Rooms</button ></div >'
    + playersPreview + '</div>';

  const activeRoomPreview = '<div class="full-screen container-md d-inline-flex align-items-stretch">'
    + leftPanelPreview + '<div class="flex-grow-1 d-inline-flex justify-content-center align-items-center card m-5" id="gameId"><button type="button" class="btn btn-primary">Not Ready</button></div>'
    + '</div>';

  root.innerHTML = activeRoomPreview;
};

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

// listener
const addRoom = () => {
  const roomName = prompt('Enter a name for the new room.').trim();
  if (!roomName) {
    alert('The name of the room is incorrect.');
  }
  socket.emit('createRoom', { username, roomName, userId: data.user.id });
};

const backToRooms = leaveData => {
  socket.emit('leaveRoom', { roomId: leaveData.activeRoom.id, userId: leaveData.user.id });
};

const onClick = async ({ target }) => {
  if (!target.dataset.event) return;
  const dataEvent = target.dataset.event;
  switch (dataEvent) {
    case 'createRoom':
      addRoom();
      break;

    case 'backToRooms':
      backToRooms(data);
      break;

    default:
      socket.emit('enterRoom', { username, userId: data.user.id, roomId: dataEvent });
  }
};

root.addEventListener('click', onClick);

socket.on('userCreated', userData => {
  data = { ...userData };
  renderPreviewRooms(data);
});

socket.on('enteredCreatedRoom', ({ room, user }) => {
  data = {
    ...data,
    user,
    rooms: [room, ...data.rooms],
    activeRoom: room
  };
  renderGameRoom(data);
});

socket.on('enteredRoom', ({ room, user }) => {
  data = {
    ...data,
    user,
    activeRoom: room
  };
  renderGameRoom(data);
});

socket.on('leavedRoom', ({ rooms, user }) => {
  data = {
    ...data,
    user,
    rooms,
    activeRoom: undefined
  };
  renderPreviewRooms(data);
});
