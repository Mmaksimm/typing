/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-console */
// import * as config from '../config/socketConfig';
import { createUser, deleteUser } from '../controllers/userController';
import {
  createRoom,
  enterRoom,
  leaveRoom
} from '../controllers/roomController';
import gameController from '../controllers/gameController';

export default io => {
  io.on('connection', async socket => {
    try {
      const { handshake: { query: { username } } } = socket;
      const userData = await createUser(username);

      socket.emit('userCreated', userData);

      socket.on('disconnect', async () => {
        const room = await deleteUser(username);
        socket.broadcast.emit('leave', room);
      });

      socket.on('createRoom', async createRoomData => {
        const response = await createRoom(createRoomData);
        socket.emit('enteredCreatedRoom', response);
        // socket.broadcast.emit('leave', username);
      });

      socket.on('enterRoom', async enterRoomData => {
        const response = await enterRoom(enterRoomData);
        socket.emit('enteredRoom', response);
      });

      socket.on('leaveRoom', async leaveRoomData => {
        const response = await leaveRoom(leaveRoomData);
        socket.emit('leavedRoom', response);
      });

      socket.on('startGame', async leaveRoomData => {
        await gameController.startGame(leaveRoomData);
        // socket.broadcast.emit('leave', username);
      });
    } catch (err) {
      // const { status = 500, message } = err;
      /*
      const error = {
        status,
        message
      };
    */
      // socket.emmit('error', error);
    }
  });
};

/*
 socket.on('createRoom', roomId => {
   socket.join(roomId);
 });
 socket.on('leaveRoom', roomId => {
   socket.leave(roomId);
 });
 */
