import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  constructor() {
    super('users');
  }
}

const userRepository = new UserRepository();
export default userRepository;
