import BaseRepository from './baseRepository';

class RoomRepository extends BaseRepository {
  constructor() {
    super('rooms');
  }
}

const roomRepository = new RoomRepository();
export default roomRepository;
