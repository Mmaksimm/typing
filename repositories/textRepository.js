import BaseRepository from './baseRepository';

class TextRepository extends BaseRepository {
  constructor() {
    super('texts');
  }
}

const textRepository = new TextRepository();
export default textRepository;
