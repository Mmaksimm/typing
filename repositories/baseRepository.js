/* eslint-disable no-console */
import { uuid } from 'uuidv4';
import dbAdapter from '../config/repositoriesConfig';

class BaseRepository {
  constructor(collectionName) {
    this.dbContext = dbAdapter.get(collectionName);
    this.collectionName = collectionName;
  }

  getAll() {
    return this.dbContext.value();
  }

  getOne(search) {
    return this.dbContext.find(search).value();
  }

  create(dataToCreate) {
    const createdAt = new Date();

    const data = {
      ...dataToCreate,
      createdAt,
      id: uuid(),
      updatedAt: createdAt
    };

    const list = this.dbContext.push(data).write();
    return list.find(it => it.id === data.id);
  }

  update(id, dataToUpdate) {
    const updatedAt = new Date();

    const data = {
      ...dataToUpdate,
      updatedAt
    };

    return this.dbContext.find({ id }).assign(data).write();
  }

  delete(id) {
    return this.dbContext.remove({ id }).write();
  }
}

export default BaseRepository;
