import path from 'path';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

const dbPath = `${path.resolve()}/database.json`;
const adapter = new FileSync(dbPath);
const dbAdapter = low(adapter);
const defaultDb = { users: [], rooms: [], texts: [] };

dbAdapter.defaults(defaultDb).write();

export default dbAdapter;
